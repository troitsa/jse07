package ru.vlasova.iteco.taskmanager.context;

import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.repository.UserRepository;
import ru.vlasova.iteco.taskmanager.service.ProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.CommandCorruptException;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.TaskService;
import ru.vlasova.iteco.taskmanager.service.UserService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator  {

    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private User currentUser;

    private final IProjectService projectService = new ProjectService(projectRepository, taskService);

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(final User currentUser) {
        this.currentUser = currentUser;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    private void registry(final AbstractCommand command) {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();

        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();

        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();

        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public void start() throws Exception {
        createUsers();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (true) {
            command = reader.readLine();
            execute(command);
        }
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        final boolean checkAccess = !abstractCommand.secure() || abstractCommand.secure() &&
                getCurrentUser() != null;
        final Role[] roles = abstractCommand.getRole();
        final boolean checkRole = abstractCommand.getRole() == null ||
                userService.checkRole(currentUser.getId(), roles);
        try {
            if (checkAccess && checkRole) {
                abstractCommand.execute();
            } else {
                System.out.println("Access denied. Please, login");
            }
        } catch (DuplicateException | IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public Bootstrap init(final Class[] classes) {
        for (Class clazz : classes) {
            try {
                registry((AbstractCommand) clazz.newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                System.out.println(e.getMessage());
            }
        }
        return this;
    }

    private void createUsers() {
        final User user = new User(Role.USER);
        user.setLogin("user");
        user.setPwd("qwerty");
        final User admin = new User(Role.ADMIN);
        admin.setLogin("admin");
        admin.setPwd("1234");
        try {
            getUserService().persist(user);
            getUserService().persist(admin);
        } catch (DuplicateException e) {
            System.out.println(e.getMessage());
        }
        setCurrentUser(user);
    }

}