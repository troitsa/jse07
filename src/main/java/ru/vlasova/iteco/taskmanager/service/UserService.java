package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    private IUserRepository repository;

    public UserService(IUserRepository repository) {
        this.repository = repository;
    }

    @Override
    public IUserRepository getRepository() {
        return repository;
    }

    @Override
    public void edit(final User user, final String login, final String password) {
        if (!isValid(login, password)) return;
        user.setLogin(login);
        user.setPwd(password);
    }

    @Override
    public User insert(final String login, final String password) {
        final boolean checkGeneral = isValid(login, password);
        if (!checkGeneral) return null;
        return new User(login, password);
    }

    @Override
    public User login(final String login, final String password) {
        final String id = checkUser(login);
        if(id == null) return null;
        final User user = repository.findOne(id);
        final String pwd = user.getPwd();
        if(pwd.equals(HashUtil.MD5(password))) {
            return user;
        }
        else {
            return null;
        }
    }

    @Override
    public String checkUser(final String login) {
        if(!isValid(login)) return null;
        final List<User> users = repository.findAll();
        for (User user : users) {
            if(login.equals(user.getLogin())) return user.getId();
        }
        return null;
    }

    @Override
    public boolean checkRole(final String userId, final Role[] roles) {
        User user = findOne(userId);
        for (Role role : roles) {
            if(user.getRole().equals(role)) return true;
        }
        return false;
    }



}
