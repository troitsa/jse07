package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.api.repository.IRepository;
import ru.vlasova.iteco.taskmanager.api.service.IService;
import ru.vlasova.iteco.taskmanager.entity.AbstractEntity;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    public abstract IRepository<E> getRepository();

    @Override
    public void merge(final E entity) {
        getRepository().merge(entity);
    }

    @Override
    public E persist(final E entity) throws DuplicateException {
        getRepository().persist(entity);
        return entity;
    }

    @Override
    public List<E> findAll() {
        return getRepository().findAll();
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null) return null;
        return getRepository().findAll(userId);
    }

    @Override
    public E findOne(final String id) {
        return getRepository().findOne(id);
    }

    @Override
    public E findOneByUserId(final String userId, final String id) {
        return getRepository().findOneByUserId(userId, id);
    }

    @Override
    public void remove(final String id) {
        if(id == null || id.trim().length() == 0) return;
        getRepository().remove(id);
    }

    @Override
    public void removeAll() {
        getRepository().removeAll();
    }

    @Override
    public void removeAll(final String userId) {
        getRepository().removeAll(userId);
    }

}
