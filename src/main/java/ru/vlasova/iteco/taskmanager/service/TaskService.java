package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.ArrayList;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private ITaskRepository repository;

    public TaskService(ITaskRepository repository) {
        this.repository = repository;
    }

    @Override
    public ITaskRepository getRepository() {
        return repository;
    }

    @Override
    public Task insert(final String userId, final String name, final String description, final String dateStart, final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral) return null;
        final Task task = new Task(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateUtil.parseDateFromString(dateStart));
        task.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return task;
    }

    @Override
    public void remove(final String userId, final String taskId) {
        if (taskId == null || userId == null) return;
        repository.remove(taskId);
    }

    @Override
    public void remove(final String userId, final int id) {
        if (userId == null || id < 0) return;
        final Task task = getTaskByIndex(userId, id);
        if (task == null) return;
        remove(userId, task.getId());
    }

    @Override
    public Task getTaskByIndex(final String userId, final int index) {
        if (userId == null || index < 0) return null;
        return repository.findAll(userId).get(index);
    }

    @Override
    public void removeTasksByProjectId(final String userId, final String projectId) {
        if (userId == null || projectId == null) return;
        final List<Task> taskList = getTasksByProjectId(userId, projectId);
        for (Task task : taskList) {
            repository.remove(task.getId());
        }
    }

    @Override
    public  List<Task> getTasksByProjectId(final String userId, final String projectId) {
        if (userId == null || projectId == null || projectId.trim().isEmpty()) return null;
        final List<Task> taskList = new ArrayList<>();
        final List<Task> tasks = findAll(userId);
        for (Task task : tasks) {
            if (task.getProjectId().equals(projectId)) {
                taskList.add(task);
            }
        }
        return taskList;
    }

}
