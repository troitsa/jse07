package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private IProjectRepository repository;

    private ITaskService taskService;

    public ProjectService(final IProjectRepository repository, final ITaskService taskService) {
        this.repository = repository;
        this.taskService = taskService;
    }

    public void setTaskService(final TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public IProjectRepository getRepository() {
        return repository;
    }

    @Override
    public Project insert(final String userId, final String name, final String description, final String dateStart, final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral) return null;
        final Project project = new Project(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(DateUtil.parseDateFromString(dateStart));
        project.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return project;
    }

    @Override
    public void remove(final String userId, final int index) {
        if (userId == null) return;
        final Project project = getProjectByIndex(userId, index);
        if (project == null) return;
        repository.remove(userId, project.getId());
        taskService.removeTasksByProjectId(userId, project.getId());
    }

    @Override
    public void remove(final String userId, final String id) {
        final Project project = findOneByUserId(userId, id);
        if(project == null) return;
        repository.remove(userId, project.getId());
        taskService.removeTasksByProjectId(userId, project.getId());
    }

    @Override
    public Project getProjectByIndex(final String userId, final int index) {
        return repository.findAll(userId).get(index);
    }

    @Override
    public List<Task> getTasksByProjectIndex(final String userId, final int projectIndex) {
        if (userId == null || projectIndex < 0) return null;
        final List<Project> projectList = findAll(userId);
        final String projectId = projectList.get(projectIndex).getId();
        return taskService.getTasksByProjectId(userId, projectId);
    }

}
