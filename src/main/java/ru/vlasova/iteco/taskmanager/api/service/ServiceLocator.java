package ru.vlasova.iteco.taskmanager.api.service;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;

import java.util.List;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    User getCurrentUser();

    void setCurrentUser(final User user);

    List<AbstractCommand> getCommands();

}
