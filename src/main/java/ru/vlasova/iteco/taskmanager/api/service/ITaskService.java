package ru.vlasova.iteco.taskmanager.api.service;

import ru.vlasova.iteco.taskmanager.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task insert(String userId, String name, String description, String dateStart, String dateFinish);

    Task getTaskByIndex(String userId, int index);

    void removeTasksByProjectId(String userId, String projectId);

    List<Task> getTasksByProjectId(String userId, String projectId);

    void remove(String userId, String id);

    void remove(String userId, int index);

}
