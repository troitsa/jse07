package ru.vlasova.iteco.taskmanager.api.repository;

import ru.vlasova.iteco.taskmanager.entity.Project;

public interface IProjectRepository extends IRepository<Project> {
}
