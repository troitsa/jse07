package ru.vlasova.iteco.taskmanager.api.repository;

import ru.vlasova.iteco.taskmanager.entity.Task;

public interface ITaskRepository extends IRepository<Task> {
}
