package ru.vlasova.iteco.taskmanager.api.repository;

import ru.vlasova.iteco.taskmanager.entity.AbstractEntity;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    List<E> findAll(String userId);

    E findOne(String id);

    E findOneByUserId(String userId, String id);

    E persist(E obj) throws DuplicateException;

    void merge(E obj);

    void remove(String id);

    void remove(String userId, String id);

    void removeAll();

    void removeAll(String userId);
}
