package ru.vlasova.iteco.taskmanager.api.repository;

import ru.vlasova.iteco.taskmanager.entity.User;

public interface IUserRepository extends IRepository<User> {
}
