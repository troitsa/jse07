package ru.vlasova.iteco.taskmanager.api.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;

import java.util.List;

public interface IProjectService extends IService<Project> {

    Project insert(String userId, String name, String description, String dateStart, String dateFinish);

    Project getProjectByIndex(String userId, int index);

    void remove(String userId, String id);

    void remove(String userId, int index);

    List<Task> getTasksByProjectIndex(String userId, int projectIndex);

}
