package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.entity.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

}