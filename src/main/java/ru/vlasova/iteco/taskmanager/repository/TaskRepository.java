package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.entity.Task;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

}
