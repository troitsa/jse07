package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.api.repository.IRepository;
import ru.vlasova.iteco.taskmanager.entity.AbstractEntity;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    private Map<String, E> entities = new HashMap<>();

    @Override
    public E findOne(final String id) {
        return entities.get(id);
    }

    @Override
    public E persist(final E entity) throws DuplicateException {
        if (entities.containsValue(entity)) throw new DuplicateException("Such entity exists.");
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void merge(final E entity) {
        entities.put(entity.getId(), entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> entityList = new ArrayList<>();
        for(E entity : entities.values()) {
            if(entity.getUserId().equals(userId)) {
                entityList.add(entity);
            }
        }
        return entityList;
    }

    @Override
    public E findOneByUserId(final String userId, final String id) {
        final E entity = entities.get(id);
        if(userId.equals(entity.getUserId())) return entity;
        return null;
    }

    @Override
    public void remove(final String userId, final String id) {
        final E entity = entities.get(id);
        if(userId.equals(entity.getUserId())) {
            entities.remove(id);
        }
    }

    @Override
    public void removeAll(final String userId) {
        final List<E> entities = findAll(userId);
        for (E entity : entities) {
            remove(userId, entity.getId());
        }
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public List<E> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void remove(final String id) {
        entities.remove(id);
    }

}
