package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.entity.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

}
