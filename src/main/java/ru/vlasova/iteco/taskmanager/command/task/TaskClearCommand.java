package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "task_clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getCurrentUser().getId();
        final ITaskService taskService = serviceLocator.getTaskService();
        if (userId != null) {
            taskService.removeAll(userId);
            System.out.println("All tasks deleted.");
        }
    }

}