package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.io.IOException;
import java.util.List;

public final class TaskRemoveCommand extends AbstractTaskCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "task_remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws IOException {
        final String userId = serviceLocator.getCurrentUser().getId();
        final ITaskService taskService = serviceLocator.getTaskService();
        if(userId == null) return;
        System.out.println("Choose the task and type the number");
        final List<Task> taskList = taskService.findAll(userId);
        if (taskList.size() !=0) {
            printTaskList(taskList);
            final int index = Integer.parseInt(reader.readLine()) - 1;
            final Task task = taskService.getTaskByIndex(userId, index);
            if (task != null) {
                taskService.remove(userId, task.getId());
                System.out.println("Task deleted.");
            }
        }
    }

}