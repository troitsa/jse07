package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.enumeration.Role;

public final class ProjectListCommand extends AbctractProjectCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "project_list";
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getCurrentUser().getId();
        printProjectList(userId);
    }

}