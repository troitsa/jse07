package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

public final class UserViewCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

    @Override
    public String getName() {
        return "user_view";
    }

    @Override
    public String getDescription() {
        return "Show user data";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getCurrentUser();
        if (user == null) {
            System.out.println("You are not authorized");
        }
        else {
            System.out.println(user);
        }
    }

}