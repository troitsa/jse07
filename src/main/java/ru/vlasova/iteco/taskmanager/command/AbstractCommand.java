package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public abstract boolean secure();

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException, DuplicateException;

    public Role[] getRole() {
        return null;
    }

}