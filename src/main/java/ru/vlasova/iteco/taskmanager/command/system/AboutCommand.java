package ru.vlasova.iteco.taskmanager.command.system;

import com.jcabi.manifests.Manifests;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Print about build";
    }

    @Override
    public void execute() {
        final String buildNumber = Manifests.read("buildNumber");
        final String developer = Manifests.read("developer");
        System.out.println("build number: " + buildNumber + "\n" + "developer: " + developer);
    }

}
