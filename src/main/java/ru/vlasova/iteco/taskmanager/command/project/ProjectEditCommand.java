package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.io.IOException;

public final class ProjectEditCommand extends AbctractProjectCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "project_edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected project";
    }

    @Override
    public void execute() throws IOException {
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = serviceLocator.getCurrentUser().getId();
        System.out.println("Choose the task and type the number");
        printProjectList(userId);
        final int index = Integer.parseInt(reader.readLine())-1;
        final Project project = projectService.getProjectByIndex(userId, index);
        if (project != null) {
            System.out.println("Editing task: " + project.getName() + ". Set new name: ");
            project.setName(reader.readLine());
            System.out.println("Task edit.");
        }
    }

}