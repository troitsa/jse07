package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.util.List;

public abstract class AbctractProjectCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    public void printProjectList(final String userId) {
        List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        if(projectList != null && projectList.size()!=0) {
            int i = 1;
            for (Project project : projectList) {
                System.out.println(i++ + ": " + project);
            }
        }
        else {
            System.out.println("There are no projects.");
        }
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

}
