package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.io.IOException;

public final class UserEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

    @Override
    public String getName() {
        return "user_edit";
    }

    @Override
    public String getDescription() {
        return "Edit user";
    }

    @Override
    public void execute() throws IOException {
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("Edit user. Set new login: ");
        final String login = reader.readLine();
        System.out.println("Set new password: ");
        final String password = reader.readLine();
        final User user = serviceLocator.getCurrentUser();
        userService.edit(user, login, password);
    }

}