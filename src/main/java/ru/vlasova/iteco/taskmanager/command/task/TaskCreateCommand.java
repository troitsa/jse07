package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.io.IOException;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "task_create";
    }

    @Override
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public void execute() throws IOException, DuplicateException {
        final String userId = serviceLocator.getCurrentUser().getId();
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("Creating task. Set name: ");
        final String name = reader.readLine();
        System.out.println("Input description: ");
        final String description = reader.readLine();
        System.out.println("Set start date: ");
        final String dateStart = reader.readLine();
        System.out.println("Set end date: ");
        final String dateFinish = reader.readLine();
        final Task task = taskService.insert(userId, name, description, dateStart, dateFinish);
        taskService.persist(task);
        System.out.println("Task created.");
    }

}