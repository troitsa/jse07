package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "task_list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getCurrentUser().getId();
        final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        if(taskList.size()==0) {
            System.out.println("There are no tasks.");
        }
        else {
            printTaskList(taskList);
        }
    }

}