package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;


import java.io.IOException;

public final class UserRegistrationCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    @Override
    public String getName() {
        return "user_registration";
    }

    @Override
    public String getDescription() {
        return "To register new user";
    }

    @Override
    public void execute() throws IOException, DuplicateException {
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("Creating user. Set login: ");
        final String login = reader.readLine();
        System.out.println("Set password: ");
        final String password = reader.readLine();
        final User user = userService.insert(login, password);
        if(user == null) {
            System.out.println("User is not registered. Try again");
        } else {
            userService.persist(user);
            System.out.println("User is registered.");
        }
    }

}