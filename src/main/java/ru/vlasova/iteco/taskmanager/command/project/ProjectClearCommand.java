package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

public final class ProjectClearCommand extends AbctractProjectCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "project_clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = serviceLocator.getCurrentUser().getId();
        if (userId != null) {
            projectService.removeAll(userId);
            System.out.println("All projects deleted.");
        }
    }

}