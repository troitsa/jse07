package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.io.IOException;

public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getName() {
        return "user_login";
    }

    @Override
    public String getDescription() {
        return "User authorization";
    }

    @Override
    public void execute() throws IOException {
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("Login: ");
        final String login = reader.readLine();
        System.out.println("Password: ");
        final String password = reader.readLine();
        final User user = userService.login(login, password);
        if(user != null) {
            serviceLocator.setCurrentUser(user);
            System.out.println("Log in success");
        }
        else {
            System.out.println("Login or password invalid");
        }
    }

}