package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.io.IOException;
import java.util.List;

public final class UserRemoveCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "user_remove";
    }

    @Override
    public String getDescription() {
        return "Remove user";
    }

    public Role[] getRole() {
        return new Role[] {Role.ADMIN};
    }

    @Override
    public void execute() throws IOException, DuplicateException {
        final User user = serviceLocator.getCurrentUser();
        final IUserService userService = serviceLocator.getUserService();
        if (user == null) {
            System.out.println("You are not authorized");
            return;
        }
        final List<User> userList = userService.findAll();
        System.out.println("Choose user:");
        for(User u : userList) {
            System.out.println(u.getLogin());
        }
        final String userLogin = reader.readLine();
        userService.remove(userService.checkUser(userLogin));
        System.out.println("User removed");
    }

}
