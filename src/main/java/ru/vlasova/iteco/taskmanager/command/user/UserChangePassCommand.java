package ru.vlasova.iteco.taskmanager.command.user;

import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.io.IOException;

public final class UserChangePassCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

    @Override
    public String getName() {
        return "user_change_pass";
    }

    @Override
    public String getDescription() {
        return "Change user password";
    }

    @Override
    public void execute() throws IOException {
        final IUserService userService = serviceLocator.getUserService();
        final User user = serviceLocator.getCurrentUser();
        System.out.println("Set new password: ");
        final String password = reader.readLine();
        if(password == null) {
            System.out.println("Invalid password");
        }
        else {
            userService.edit(user, user.getLogin(), password);
            System.out.println("Password changed");
        }
    }

}
