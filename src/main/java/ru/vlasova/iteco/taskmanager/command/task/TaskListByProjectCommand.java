package ru.vlasova.iteco.taskmanager.command.task;

import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.io.IOException;
import java.util.List;

public final class TaskListByProjectCommand extends AbstractTaskCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "task_list_by_project";
    }

    @Override
    public String getDescription() {
        return "Show tasks by selected project";
    }

    @Override
    public void execute() throws IOException {
        final String userId = serviceLocator.getCurrentUser().getId();
        final ITaskService taskService = serviceLocator.getTaskService();
        final IProjectService service = serviceLocator.getProjectService();
        if(printProjectList(userId) == null) return;
        System.out.println("Please, choose the project and type the number");
        final int index = Integer.parseInt(reader.readLine())-1;
        final List<Task> taskList = service.getTasksByProjectIndex(userId, index);
        if(taskList.size()==0) {
            System.out.println("There are no tasks.");
        }
        else {
            printTaskList(taskList);
        }
    }

}