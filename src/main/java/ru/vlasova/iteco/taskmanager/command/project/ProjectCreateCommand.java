package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.io.IOException;

public final class ProjectCreateCommand extends AbctractProjectCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "project_create";
    }

    @Override
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() throws IOException, DuplicateException {
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = serviceLocator.getCurrentUser().getId();
        if (userId == null) return;
        System.out.println("Creating project. Set name: ");
        final String name = reader.readLine();
        System.out.println("Input description: ");
        final String description = reader.readLine();
        System.out.println("Set start date: ");
        final String dateStart = reader.readLine();
        System.out.println("Set end date: ");
        final String dateFinish = reader.readLine();
        final Project project = projectService.insert(userId, name, description, dateStart, dateFinish);
        projectService.persist(project);
        System.out.println("Project created.");
    }

}