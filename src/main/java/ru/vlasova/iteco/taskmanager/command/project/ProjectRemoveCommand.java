package ru.vlasova.iteco.taskmanager.command.project;

import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.io.IOException;

public final class ProjectRemoveCommand extends AbctractProjectCommand {

    @Override
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    public String getName() {
        return "project_remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws IOException {
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = serviceLocator.getCurrentUser().getId();
        printProjectList(userId);
        System.out.println("To delete project choose the project and type the number");
        final int index = Integer.parseInt(reader.readLine())-1;
        projectService.remove(userId, index);
        System.out.println("Project delete.");
    }

}