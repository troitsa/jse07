package ru.vlasova.iteco.taskmanager.entity;

import java.util.Date;

public final class Task extends AbstractEntity  {

    private String name = "";

    private String description = "";

    private Date dateStart;

    private Date dateFinish;

    private String projectId = "";

    public Task() {
    }

    public Task(final String userId) {
        this.userId = userId;
    }

    public Task(final String name, final String description, final Date dateStart, final Date dateFinish) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "    Task " + name;
    }

}
