package ru.vlasova.iteco.taskmanager.entity;

import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

public final class User extends AbstractEntity {

    private String login = "";

    private String pwd = "";

    private Role role = Role.USER;

    public User() {
    }

    public User(final Role role) {
        this.role = role;
    }

    public User(final String login, final String pwd) {
        this.login = login;
        this.pwd = HashUtil.MD5(pwd);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(final String pwd) {
        this.pwd = HashUtil.MD5(pwd);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User: " + login + ", " + role;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final User user = (User) o;
        return login.equals(user.getLogin());
    }

}
