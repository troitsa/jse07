package ru.vlasova.iteco.taskmanager.entity;

import java.util.Date;

public final class Project extends AbstractEntity {

    private String name = "";

    private String description = "";

    private Date dateStart;

    private Date dateFinish;

    public Project() {
    }

    public Project(final String userId) {
        this.userId = userId;
    }

    public Project(final String name, final String description, final Date dateStart, final Date dateFinish) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Project project = (Project) o;
        return name.equals(project.getName());
    }

    @Override
    public String toString() {
        return "Project " + name;
    }

}
