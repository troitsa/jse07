package ru.vlasova.iteco.taskmanager.error;

public final class DuplicateException extends Exception {

    public DuplicateException(final String message) {
        super(message);
    }

}
